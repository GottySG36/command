// Utility to abstract a command

package command

import (
    "bufio"
    "bytes"
    "fmt"
    "log"
    "os"
    "os/exec"
    "regexp"
    "strings"
    "sync"
)

// Parse and format commands
// Creates Commands structs
// Modifies if singularity is used
// Creates task list
func Init(c, a, f, sep, sif, sa, sPath string, t int, v bool) (Tasks, error){
    // First we read the argument file, this will 
    // determiner how many jobs we need to run
    af, err := ReadArgumentFile(f, sep)
    if err != nil {
        return Tasks{}, err
    }

    // Creating task list with len(af) jobs and t threads
    tk := InitTasks(len(af), t, sif, v)

    // For every file argument, we...
    for jId, fArg := range af {
        // Properly format the argument string and split
        arg, err := ParseArguments(a, fArg...)
        if err != nil {
            return tk, err
        }

        // Create the job 
        tk.jobs[jId], err = NewCommand(c, sif, arg...)
        if err != nil {
            return tk, err
        }
        if sif != "" {
            // Add singularity if needed/requested
            err = tk.jobs[jId].UseSingularity(sif, "run", sa, sPath)
            if err != nil {
                return tk, err
            }
        }
    }
    return tk, nil
}

// Struct used to build and store a commandline
// It is a simple wrapper of exec.Cmd that will allow the addition 
// of a few more functionnalities
// Exec  : exec module command that will actually run the command
// Cmd   : The command path
// Args  : The list of arguments to the command
// Error : Will store the error returned by the command if any
// Sing  : Singularity struct describing the usage of singularity / docker 
//         containers through singularity (see : https://sylabs.io/docs/)
type Command struct {
    exec    *exec.Cmd
    cmd     string
    args    []string
    output  *bytes.Buffer
    err     *bytes.Buffer
    sing    *Singularity
}

// Creates a new command struct and passes the appropriate information
// to the underlying exec.Cmd.
func NewCommand(c, s string, a ...string) (*Command, error) {
    cmd := Command{}
    cPath := c
    var err error
    cmd.exec = &exec.Cmd{
                Path: c,
                Args: append([]string{c}, a...),
                }
    if s == "" {
        cPath, err = exec.LookPath(c)
        if err != nil {
            return &cmd, err
        }
        cmd.exec = exec.Command(cPath, a...)
    }

    cmd.cmd  = cPath
    cmd.args = a
    cmd.output = new(bytes.Buffer)
    cmd.err    = new(bytes.Buffer)
    cmd.exec.Stderr = cmd.err
    cmd.exec.Stdout = cmd.output

    return &cmd, nil
}

// Tells the command to use a singularity container. Changes the underlying exec.Cmd
// by updating the arguments and the command path
// By default, it will look in your PATH for it unless you provided a full path
func (c *Command) UseSingularity(s, m, a, p string) error {
    sing := Singularity{sif: s, mode: m, args: SplitArgs(a)}
    c.sing = &sing
    newArgs := make([]string, 2, 4 + len(m) + len(c.args))

    sPath, err := exec.LookPath("singularity")
    if p != "" {
        sPath, err = exec.LookPath(p)
    }
    if err != nil {
        return err
    }
    newArgs[0] = sPath
    newArgs[1] = sing.mode
    newArgs = append(newArgs, sing.args...)
    newArgs = append(newArgs, sing.sif)
    newArgs = append(newArgs, c.cmd)
    newArgs = append(newArgs, c.args...)

    c.exec.Args = newArgs
    c.exec.Path = sPath
    return nil
}

// Starts the underlying exec.Cmd
func (c *Command) Run() {
    err := c.exec.Run()
    if err != nil {
        c.err.WriteString((fmt.Sprintf("%v - %v", err, c.err)))
    }
}

// Get the output from the underlying exec.Cmd
func (c *Command) Output() (*bytes.Buffer) {
    return c.output
}

// Get the output from the underlying exec.Cmd
func (c *Command) Error() error {
    if c.err.String() == "" {
        return nil
    }
    return fmt.Errorf(c.err.String())
}

// Get the output from the underlying exec.Cmd
func (c *Command) Status() int {
    return c.exec.ProcessState.ExitCode()
}

// Since we constantly work with singularity/docker containers
// This struct is used to represent the container used
// Sif  : Filepath to a singularity image file or docker image
//        i.e. : docker://hello-world
// Mode : The usage mode for singularity. Since we want to run commands from
//        the container directly, we use `run`. Should always be set to `run`.
// Args : Arguments specific to singularity
//
// If used, these will be prefixed to our command in order to use the container 
// instead of what's available in your $PATH.
type Singularity struct {
    sif     string
    mode    string
    args    []string
}

// Struct that will be used to list all commands to run.
// This allows to group outputs on a per job basis and run commands
// with at most X `Threads` at a time. 
type Tasks struct {
    jobs        []*Command
    threads     int
    singularity bool
    verbose     bool
}

// Initiates the Task struct with j Jobs and t Threads
func InitTasks(j, t int, s string, v bool) Tasks {
    var tk Tasks
    tk.jobs = make([]*Command, j)
    tk.threads = t
    if s != "" {
        tk.singularity = true
    }
    tk.verbose = v
    return tk
}

func (t *Tasks) AddJob(c *Command, idx int) {
    t.jobs[idx] = c
}

//var spaces = regexp.MustCompile(`\s+`)
var spaces = regexp.MustCompile(`[^\s"']+|"([^"]*)"|'([^']*)`)
var words  = regexp.MustCompile("'.+'|\".+\"|\\S+")
var trailing = regexp.MustCompile(`(^\s+['\"])|(\s+['\"]$)|(^['\"])|(['\"]$)`)
var missing = regexp.MustCompile(`%!.\(MISSING\)`)
var extra = regexp.MustCompile(`%!\(EXTRA\s.+\)`)

func SplitArgs(a string) []string {
    m := words.FindAllString(a, -1)
    for idx, i := range m {
        m[idx] = trailing.ReplaceAllLiteralString(i, "")
    }
    return m
}

// Takes an argument string with format specifiers and a slice used to
// substitute the values in the format string. 
func ParseArguments(as string, fa ...interface{}) ([]string, error) {
    af := fmt.Sprintf(as, fa...)
    if missing.MatchString(af) || extra.MatchString(af) {
        return nil, fmt.Errorf("InvalidArgumentFormat : %v", af)
    }
    sArgs := SplitArgs(af)
    return sArgs, nil
}

func ReadArgumentFile(f, sep string) ([][]interface{}, error) {
    i, err := os.Open(f)
    defer i.Close()
    if err != nil {
        return nil, err
    }
    c := make([][]interface{}, 0)
    ai := make([]interface{}, 0)
    s := bufio.NewScanner(i)
    for s.Scan() {
        if s.Text() == "" {continue}
        ai = make([]interface{}, 0)
        for _, val := range strings.Split(s.Text(), sep) {
            ai = append(ai, val)
        }
        c = append(c, ai)
    }
    return c, nil
}

func (c *Command) PrintJob(nb int) {
    log.Printf(`
Starting task #%v :
    - Command : %q
    - Args    : %q
`, nb, c.cmd, c.args)
}

func (c *Command) PrintSingularityJob(nb int) {
    log.Printf(`
Starting task #%v :
    - Command       : %q
    - Args          : %q
    - Singularity   : %q
    - Sing. args    : %q
    - Sing. mode    : %q
`, nb, c.cmd, c.args, c.sing.sif, c.sing.args, c.sing.mode)
}

// Run all tasks in parallel with at most t.Threads at a time
func (t *Tasks) RunParallel() {
    if t.verbose {
        log.Printf("We have %v tasks to run and are allowed to run %v at a time.\n",
                len(t.jobs), t.threads)
    }
    threads := make(chan struct{}, t.threads)
    var wg sync.WaitGroup

    for i, cmd := range t.jobs {
        wg.Add(1)
        threads <- struct{}{}
        go func(c *Command, jNb int, s, v bool){
            defer func() {
                wg.Done()
                <-threads
            }()
            if v {
                if !s {  c.PrintJob(jNb+1)
                } else { c.PrintSingularityJob(jNb+1) }
            }
            c.Run()
            if v {log.Printf("Done job #%v\n", jNb+1)}
        }(cmd, i, t.singularity, t.verbose)
    }
    wg.Wait()
    close(threads)
}

func (t *Tasks) CheckErrors() []*error {
    Err := make([]*error, 0)
    for jId, c := range t.jobs {
        if ( c.Status() != 0 ) {
            err := c.Error()
            Err = append(Err, &err)
            log.Printf("Job #%d exit with none zero status %q err : %v\n", jId, c.Status(), c.Error())
        }
    }
    switch len(Err) {
    case 0:
        return nil
    case len(t.jobs):
        log.Printf("All jobs exitted with an error. See above for more details.\n")
        return Err
    default:
        log.Printf("Some jobs (%d) exitted with an error. See above for more details.\n", len(Err))
        return Err
    }
}

func (t *Tasks) GetStatus(id int) int {
    return t.jobs[id].Status()
}

func (t *Tasks) GetOutput(id int) string {
    return t.jobs[id].Output().String()
}

func (t *Tasks) GetErrorOutput(id int) error {
    return t.jobs[id].Error()
}

func (t *Tasks) WriteSingleOutput(f string) error {
    o, err := os.Create(fmt.Sprintf("%s.out", f))
    defer o.Close()
    if err != nil {
        return err
    }
    for _, c := range t.jobs {
        _, err := o.WriteString(c.Output().String())
        if err != nil {
            return err
        }
    }
    return nil
}

func (t *Tasks) WriteMultOutput(f string) error {
    for jId, c := range t.jobs {
        o, err := os.Create(fmt.Sprintf("%s-%d.out", f, jId))
        defer o.Close()
        if err != nil {
            return err
        }
        _, err = o.WriteString(c.Output().String())
        if err != nil {
            return err
        }
    }
    return nil
}

func (t *Tasks) WriteSingleErrorOutput(f string) error {
    o, err := os.Create(fmt.Sprintf("%s.err", f))
    defer o.Close()
    if err != nil {
        return err
    }
    for _, c := range t.jobs {
        _, err := o.WriteString(c.err.String())
        if err != nil {
            return err
        }
    }
    return nil
}

func (t *Tasks) WriteMultErrorOutput(f string) error {
    for jId, c := range t.jobs {
        o, err := os.Create(fmt.Sprintf("%s-%d.err", f, jId))
        defer o.Close()
        if err != nil {
            return err
        }
        _, err = o.WriteString(c.err.String())
        if err != nil {
            return err
        }
    }
    return nil
}
